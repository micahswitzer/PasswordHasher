/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package passwordhasher;

import java.security.MessageDigest;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Switzer & Entner
 */
public class Guess {
    //Substitutions to think about
    //! or 1 = i or l, @ = a, $ or 5 = s, 0 = o, + or 7 = t, 
    //# = h, 3 = e, 6 = g, 8 or & = b
    
    //1st pass of just looking through Bible words (Upper and lower case)
    public static void guess_normal (String[] inHash, String[] freq,
            String[] salt, String[] username, WordCombo wc,
            WordDelegator wd, MessageDigest md5) {
        //Use guess_normal and replace certain letters with common subs
        String input;
        StringBuilder testCase;
        //Test upper/lower cases
        for (int i = 0; i < freq.length; i++) {
            //if (i % 4000 == 0) {
                //System.out.println("guess_normal progress: " + i);
            //}
            input = freq[i];
            int numCombos = (int) Math.pow(2, input.length());
            for (int j = 0; j < 2; j++) {
                testCase = new StringBuilder().append(wc.prepend);
                for (int k = 0; k < input.length(); k++) {
                    int bit = (j >> k) % 2;
                    if (bit == 1) {
                        testCase.append((char)(input.charAt(k) - (char)32));
                    }
                    else {
                        testCase.append(input.charAt(k));
                    }
                }
                Program.TryPassword(username, salt, inHash,
                        testCase.append(wc.append).toString(), wd, md5);
            }
        }        
    }
    
    //2nd pass of common substitutions
    public static void guess_sub (String[] inHash, String[] freq, String[] salt,
            String[] username, Object[] subs, WordCombo wc,
            WordDelegator wd, MessageDigest md5) {
        //Use guess_normal and replace certain letters with common subs
        String input;
        StringBuilder testCase;
        //Test upper/lower cases     
        for (int i = 0; i < freq.length; i++) {
            //if (i % 4000 == 0) {
                //System.out.println("guess_sub progress: " + i);
            //}
            input = freq[i];
            //int numCombos = (int) Math.pow(2, input.length());
            int numReplaceCombos = (int) Math.pow(2, subs.length);
            for (int m = 0; m < numReplaceCombos; m++) {
                String replaced = input;
                for (int n = 0; n < subs.length; n++) {
                    int bit = (m >> n) % 2;
                    if (bit == 1) {
                        Entry<String, String> set = (Entry<String, String>) subs[n];
                        if (!replaced.contains(set.getKey())) continue;
                        replaced = replaced.replaceAll(set.getKey(), set.getValue());
                    }
                }
                for (int j = 0; j < 2; j++) { //numCombos; j++) {
                    testCase = new StringBuilder().append(wc.prepend);
                    for (int k = 0; k < replaced.length(); k++) {
                        char inputChar = replaced.charAt(k);
                        int bit = (j >> k) % 2;
                        if (bit == 1 && (inputChar >= 'a' && inputChar <= 'z')) {
                            testCase.append((char)(inputChar - (char)32));
                        }
                        else {
                            testCase.append(inputChar);
                        }
                    }
                    Program.TryPassword(username, salt, inHash,
                            testCase.append(wc.append).toString(), wd, md5);
                }
            }
        }
    }
    
    //3rd pass of common subs with appended/preppended chars
    public static void guess_final(WordDelegator wd) {
        //Use guess_sub and attach new chars to front/back of string
        
        String[] wide1 = buildDict.build1wide();
        String[] wide2 = buildDict.build2wide();
        String[] wide3 = buildDict.build3wide();
        
        //guess_normal(inHash, freq, salt, username, null, null);
        //wd.EnqueueWordCombo(null, null);
        
        //Run through all combinations with guess_normal
        
        /*
        for (int i = 0; i < wide1.length; i++) {
            if (i % 5 == 0) {
                System.out.println("wide1 normal, i = " + i);
            }
            guess_normal(inHash, freq, salt, username, null, wide1[i]);
            guess_normal(inHash, freq, salt, username, wide1[i], null);
            for (int j = 0; j < wide1.length; j++) {
                if (j % 5 == 0) {
                    System.out.println("wide1 normal, j = " + j);
                }
                guess_normal(inHash, freq, salt, username, wide1[i], wide1[j]);
            }
        }
        
        
        
        System.out.println("Block 1");
        
        for (int i = 0; i < wide1.length; i++) {
            if (i % 5 == 0) {
                System.out.println("i = " + i);
            }
            wd.EnqueueWordCombo(null, wide1[i]);
            wd.EnqueueWordCombo(wide1[i], null);
        }
        
        
        System.out.println("Block 3");
        
        for (int i = 0; i < wide2.length; i++) {
            if (i % 25 == 0) {
                System.out.println("i = " + i);
            }
            wd.EnqueueWordCombo(null, wide2[i]);
            wd.EnqueueWordCombo(wide2[i], null);
        }
        
        
        
        System.out.println("Block 5");
        
        for (int i = 0; i < wide3.length; i++) {
            if (i % 10 == 0) {
                System.out.println("i = " + i);
            }
            wd.EnqueueWordCombo(null, wide3[i]);
            wd.EnqueueWordCombo(wide3[i], null);
        }
        
        //Run through all combinations with guess_sub
        //Removed for quicker testing. Add back for final crunch...
        
        System.out.println("Block 9");
        
        for (int j = 0; j < wide1.length; j++) {
            if (j % 10 == 0) {
                System.out.println("j = " + j);
            }
            for (int i = 0; i < wide2.length; i++) {
                if (i % 10 == 0) {
                    System.out.println("i = " + i);
                }
                wd.EnqueueWordCombo(wide1[j], wide2[i]);
            }
            for (int i = 0; i < wide3.length; i++) {
                if (i % 10 == 0) {
                    System.out.println("i = " + i);
                }
                wd.EnqueueWordCombo(wide1[j], wide3[i]);
            }
        }
        */
        
        
        System.out.println("Block 10");
        
        for (int j = 0; j < wide2.length; j++) {
            if (j % 10 == 0) {
                System.out.println("j = " + j);
            }
            for (int i = 0; i < wide1.length; i++) {
                if (i % 10 == 0) {
                    System.out.println("i = " + i);
                }
                wd.EnqueueWordCombo(wide2[j], wide1[i]);
            }
            /*
            for (int i = 0; i < wide2.length; i++) {
                if (i % 10 == 0) {
                    System.out.println("i = " + i);
                }
                wd.EnqueueWordCombo(wide2[j], wide2[i]);
            }
            for (int i = 0; i < wide3.length; i++) {
                if (i % 10 == 0) {
                    System.out.println("i = " + i);
                }
                wd.EnqueueWordCombo(wide2[j], wide3[i]);
            }
            */
        }
        
        /*
        System.out.println("Block 11");
        
        for (int j = 0; j < wide3.length; j++) {
            if (j % 10 == 0) {
                System.out.println("j = " + j);
            }
            for (int i = 0; i < wide1.length; i++) {
                if (i % 10 == 0) {
                    System.out.println("i = " + i);
                }
                wd.EnqueueWordCombo(wide3[j], wide1[i]);
            }
            for (int i = 0; i < wide2.length; i++) {
                if (i % 10 == 0) {
                    System.out.println("i = " + i);
                }
                wd.EnqueueWordCombo(wide3[j], wide2[i]);
            }
            for (int i = 0; i < wide3.length; i++) {
                if (i % 10 == 0) {
                    System.out.println("i = " + i);
                }
                wd.EnqueueWordCombo(wide3[j], wide3[i]);
            }
        }
        */
    }
}
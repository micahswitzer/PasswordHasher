/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordhasher;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 *
 * @author Micah Switzer
 */
public class WordDelegator {
    private Queue<WordCombo> wordQueue;
    private Queue<HashSuccess> successQueue;
    
    public WordDelegator() {
        wordQueue = new ConcurrentLinkedQueue<>();
        successQueue = new ConcurrentLinkedQueue<>();
    }
    
    public void EnqueueWordCombo(String pre, String app) {
        WordCombo wc = new WordCombo();
        wc.append = app == null ? "" : app;
        wc.prepend = pre == null ? "" : pre;
        wordQueue.add(wc);
    }
    public WordCombo DequeueWordCombo() {
        return wordQueue.remove();
    }
    
    public void EnqueueSuccess(HashSuccess hs) {
        successQueue.add(hs);
    }
    public HashSuccess DequeueSuccess() {
        return successQueue.remove();
    }
    
    public boolean HasWords() {
        return !wordQueue.isEmpty();
    }
    public boolean HasSuccess() {
        return !successQueue.isEmpty();
    }
}

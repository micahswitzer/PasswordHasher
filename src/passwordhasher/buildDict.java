/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordhasher;

/**
 *
 * @author Home
 */
public class buildDict {
    
    private static final int OFFSET = 31;
    
    // fills a string array with all 93 printable chars in ascii
    public static String[] build1wide () {
	String[] wide1 = new String[OFFSET];

	char insert = '!';
	for (int i = 0; i < OFFSET; i++) {
		wide1[i] = String.valueOf(insert);
		insert++;
	}
        return (wide1);
    }

    // fills a string array with 93^2 words, all permutations of 2 chars in ascii
    public static String[] build2wide () {
	String[] wide2 = new String[OFFSET * OFFSET];

	char insert1 = '!';
	char insert2 = '!';
	int count = 0;
	for (int i = 0; i < OFFSET; i++) {
            for (int j = 0; j < OFFSET; j++) {
                StringBuilder sb = new StringBuilder(2);
                sb.append(insert1).append(insert2);
                wide2[count++] = sb.toString();
                insert2 += 1;
            }
            insert1 += 1;
	}
        return (wide2);
    }

    // fills wide3
    public static String[] build3wide () {
	String[] wide3 = new String[OFFSET * OFFSET * OFFSET];

	char insert1 = '!';
	char insert2 = '!';
	char insert3 = '!';
	int count = 0;
	for (int i = 0; i < OFFSET; i++) {
            for (int j = 0; j < OFFSET; j++) {
                for (int k = 0; k < OFFSET; k++) {
                    StringBuilder sb = new StringBuilder(3);
                    sb.append(insert1).append(insert2).append(insert3);
                    wide3[count++] = sb.toString();
                    insert3++;
                }
                insert2++;
            }
            insert1++;
	}
        return(wide3);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordhasher;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/**
 *
 * @author Micah Switzer
 */
public class WordGuesser {
    
    String[] hashes;
    String[] users;
    String[] salts;
    String[] dict;
    Map<String, String> subs;
    WordDelegator wd;
    Object[] newSubs;
    MessageDigest md5;
    
    public WordGuesser(String[] dict, String[] users, String[] salts, String[] hashes, Map<String, String> subs, WordDelegator wd) {
        this.dict = dict;
        this.hashes = hashes;
        this.salts = salts;
        this.subs = subs;
        this.users = users;
        this.wd = wd;
        this.newSubs = subs.entrySet().toArray();
        try {
            md5 = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException ex) { }
    }
    
    public void GuessPlain(WordCombo wc) {
        Guess.guess_normal(hashes, dict, salts, users, wc, wd, md5);
    }
    
    public void GuessSub(WordCombo wc) {
        Guess.guess_sub(hashes, dict, salts, users, newSubs, wc, wd, md5);
    }
    
}

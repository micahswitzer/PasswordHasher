/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordhasher;

/**
 *
 * @author Micah Switzer
 */
public class WordTrialThread implements Runnable {

    WordDelegator wordDelegator;
    WordGuesser wordGuesser;
    
    public WordTrialThread(WordDelegator wd, InputParser ip) {
        wordDelegator = wd;
        wordGuesser = new WordGuesser(ip.GetWords(), ip.GetUsers(),
                ip.GetSalts(), ip.GetHashes(), ip.GetReplacementMap(), wd);
    }
    
    @Override
    public void run() {
        //long threadId = Thread.currentThread().getId();
        while (wordDelegator.HasWords()) {
            //System.out.println("Thread " + threadId + " starting new word");
            WordCombo wc = wordDelegator.DequeueWordCombo();
            wordGuesser.GuessPlain(wc);
            wordGuesser.GuessSub(wc);
        }
    }
    
}

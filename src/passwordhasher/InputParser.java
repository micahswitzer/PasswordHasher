/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordhasher;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Micah Switzer
 */
public class InputParser {
    
    private Map<String, Integer> dict;
    private List<Entry<String, Integer>> lst;
    private String[] users;
    private String[] hashes;
    private String[] salts;
    private Map<String, String> replace;
    
    public InputParser() {
        
    }
    
    public void ParseInputDictionary(String fileName) throws FileNotFoundException {
        this.dict = new HashMap<>();
        File file = new File(fileName);
        Scanner s = new Scanner(file).useDelimiter("\\Z");
        String input = s.next();
        Pattern p = Pattern.compile("[\\w']+");
        Matcher m = p.matcher(input);
        while (m.find()) {
            String word = input.substring(m.start(), m.end()).toLowerCase();
            dict.put(word, dict.getOrDefault(word, 0) + 1);
        }
        Set<Entry<String, Integer>> c = this.dict.entrySet();
        lst = new ArrayList<>(c);
        Collections.sort(lst, (Entry<String, Integer> o1, Entry<String, Integer> o2) -> o2.getValue().compareTo(o1.getValue()));
    }
    public String[] GetWords() {
        String[] result = new String[lst.size()];
        int idx = 0;
        for (Entry<String, Integer> e: lst) {
            result[idx++] = e.getKey();
        }
        return result;
    }
    
    public void ParseInputHashes(String fileName, int expNum) throws FileNotFoundException {
        users = new String[expNum];
        hashes = new String[expNum];
        salts = new String[expNum];
        File file = new File(fileName);
        Scanner s = new Scanner(file);
        int i = 0;
        while (s.hasNextLine()) {
            String line = s.nextLine();
            String[] pieces = line.split(":");
            users[i] = pieces[0];
            salts[i] = pieces[1].equals("") ? null : pieces[1];
            hashes[i] = pieces[2];
            i++;
        }
    }
    public String[] GetUsers() {
        return users;
    }
    public String[] GetSalts() {
        return salts;
    }
    public String[] GetHashes() {
        return hashes;
    }
    
    public void ParseReplacements(String fileName) throws FileNotFoundException {
        this.replace = new HashMap<>();
        File file = new File(fileName);
        Scanner s = new Scanner(file);
        int i = 0;
        while (s.hasNextLine()) {
            String line = s.nextLine();
            String[] pieces = line.split(":");
            if (pieces.length != 2) continue;
            replace.put(pieces[0], Matcher.quoteReplacement(pieces[1]));
        }
    }
    public Map<String, String> GetReplacementMap() {
        return this.replace;
    }
}

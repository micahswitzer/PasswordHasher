/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordhasher;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Switzer & Entner
 */
public class Program {

    private static long startTime = 0;
    private static final String OUTPUT_FILE_PATH = "output.txt";
    
    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        InputParser parser = new InputParser();
        parser.ParseInputDictionary("Bible.txt");
        String[] words = parser.GetWords();
        parser.ParseInputHashes("pa3hashes.txt", 5);
        parser.ParseReplacements("replacements.txt");
        
        startTime = System.currentTimeMillis();
        
        WordDelegator wd = new WordDelegator();
                
        Guess.guess_final(wd); // attempt to queue all possible words
        ExecutorService pool = Executors.newFixedThreadPool(8);
        for (int i = 0; i < 8; i++) {
            pool.execute(new WordTrialThread(wd, parser));
        }
        while (wd.HasWords()) {
            while (wd.HasSuccess()) {
                HashSuccess hs = wd.DequeueSuccess();
                WriteSuccessMessage(hs.userName, hs.password, hs.time);
            }
            try {
                Thread.sleep(500);
            }
            catch (InterruptedException ex) {
            }
        }
        pool.shutdown();
    }
    
    public static void WriteSuccessMessage(String userName, String password, long currentTime)
    {
        String line = userName + " " + password + " " + GetTimeString(currentTime - startTime) + "\n";
        try (FileWriter outputFile = new FileWriter(OUTPUT_FILE_PATH, true)) {
            outputFile.write(line);
            outputFile.close();
        } catch (IOException ex) {
            System.out.println(line);
        }
    }
    
    private static String  GetTimeString(long time) {
        long remaining = time;
        long hours = remaining / (1000 * 60 * 60);
        remaining %= 1000 * 60 * 60;
        long minutes = remaining / (1000 * 60);
        remaining %= 1000 * 60;
        long seconds = remaining / 1000;
        remaining %= 1000;
        return hours + ":" + minutes + ":" + seconds + ":" + remaining;
    }
    
    public static void TryPassword(String[] userNames, String[] salts, String[] hashes, String plaintext, WordDelegator wd, MessageDigest md5) 
    {
        String noSalt = ComputeHash(plaintext, md5);
        for (int i = 0 ; i < userNames.length; i++)
        {
            //String currentHash;
            //if (salts[i] == null || salts[i].equals(""))
            //{
            //    currentHash = noSalt;
            //}
            //else
            //{
            //    currentHash = ComputeHash(plaintext + salts[i], md5);
            //}
            if (hashes[i].equals(noSalt))
            {
                HashSuccess hs = new HashSuccess();
                hs.password = plaintext;
                hs.userName = userNames[i];
                hs.time = System.currentTimeMillis();
                wd.EnqueueSuccess(hs);
                //WriteSuccessMessage(userNames[i], plaintext);
            }
        }
    }
    
    private static String ComputeHash(String plaintext, MessageDigest md5)
    {
        byte[] hash = md5.digest(plaintext.getBytes());
        BigInteger hashResult = new BigInteger(1, hash);
        String hashString = hashResult.toString(16);
        while (hashString.length() < 32) {
            hashString = "0" + hashString;
        }
        return hashString;
    }
}
